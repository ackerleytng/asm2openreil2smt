# Quickstart guide

## Install dependencies

+ [sde](https://software.intel.com/en-us/articles/pre-release-license-agreement-for-intel-software-development-emulator-accept-end-user-license-agreement-and-download)
+ [z3](http://z3.codeplex.com/SourceControl/latest#README)
+ [openreil](https://github.com/Cr4sh/openreil)

## Compile vulnerable program

```
cd vulnerable
make
```

Move it to the asm2openreil2smt directory

```
mv vulnerable/vulnerable ./tmp_target
```

## System configuration for script to run

```
sudo sh -c 'echo 0 > /proc/sys/kernel/yama/ptrace_scope'
sudo sh -c 'echo 0 > /proc/sys/kernel/randomize_va_space'
```

## Run it

```
python main.py
```

You might get an error like this:

```
user@ubuntu:~/asm2openreil2smt$ python main.py 
[+] Trying input: good
[+] Tracing...
[+] Running sde...
[+] Parsing trace...
[+] Updated tree: (0x08048552:1 (0x08048563:1 (0x08048574:1 (0x08048585:1 (?:1 () ()) ()) ()) ()) ())
[+] Solving for: 0x08048585, False
Traceback (most recent call last):
  File "main.py", line 115, in <module>
    new_input[solved_address - memory_offset_for_input] = chr(all_results[k])
IndexError: list assignment index out of range
```

If you see this, you have to manually update this variable (sorry)

```
#----------------------------------------------------------------------
# Script begins here
# There is a significant amount of hard coding in this script
#   If this is to really be applied, more automation should be done
#----------------------------------------------------------------------
memory_offset_for_input = 0xbffff2e7                        <---------------- Find this line
inputs = ["good"]
record = ExplorationRecord()
```

Then open `tmp_sdetrace.dat` and search for `tmp_target:test`, and find this line:

```
TID0: INS 0x0804853d tmp_target:test     BASE     push ebp                                 | esp = 0xbffff2c8
TID0: Write *(UINT32*)0xbffff2c8 = 0xbffff2f8
TID0: INS 0x0804853e tmp_target:test+0x0001 BASE     mov ebp, esp                          | ebp = 0xbffff2c8
TID0: INS 0x08048540 tmp_target:test+0x0003 BASE     sub esp, 0x28                         | esp = 0xbffff2a0, eflags = 0x286
TID0: INS 0x08048543 tmp_target:test+0x0006 BASE     mov dword ptr [ebp-0xc], 0x0
TID0: Write *(UINT32*)0xbffff2bc = 0
TID0: Read 0xbffff2e7 = *(UINT32*)0xbffff2d0
TID0: INS 0x0804854a tmp_target:test+0x000d BASE     mov eax, dword ptr [ebp+0x8]          | eax = 0xbffff2e7
TID0: Read 0x62 = *(UINT8*)0xbffff2e7
TID0: INS 0x0804854d tmp_target:test+0x0010 BASE     movzx eax, byte ptr [eax]             | eax = 0x62
```

This is the beginning of where the vulnerable function is loaded. It reads from the file, and the memory address of the first character, `0x62` in this case, is `0xbffff2e7`.

Assign `0xbffff2e7` to `memory_offset_for_input`.

Run it again

```
python main.py
```
