from pprint import pprint


class MemoryAccess(object):
    def __init__(self, address, size, data):
        self.address = address
        self.size    = size
        self.data    = data

class MemoryRead(MemoryAccess):
    def __init__(self, address, size, data):
        self.address = address
        self.size    = size
        self.data    = data

class MemoryWrite(MemoryAccess):
    def __init__(self, address, size, data):
        self.address = address
        self.size    = size
        self.data    = data

class Event(object):
    """ Represents a trace event """

    def __init__(self):
        self.status = "INITIALIZED"
        self.tid = None
        self.modified = None
        self.disasm = None
        self.address = None
        self.reads = []
        self.writes = []
        self.path = ""

    def dump(self):
        print "-----evt-----"
        print ">>", self.disasm
        if self.address:
            print "address", hex(self.address)
        else:
            print "address", self.address
        print "status", self.status
        print "path", self.path
        print "tid", self.tid
        print "modified", self.modified
        print "read", self.reads
        print "write", self.writes
        print "---- evt done"

class Trace(object):
    """ Represents the trace file """

    def __init__(self, file_path):
        """ Initialization of the Trace object """
        self.file_iter = open(file_path)
        self.saved_line = None
        self.evt = None
        self.processed = False
        self.path = file_path #save the file path for reset operations
        self.event_id = 0

    def reset(self):
        """ Reset the trace events and file handle """
        self.file_iter = open(self.path)
        self.saved_line = None
        self.evt = None
        self.processed = False
        self.event_id = 0

    def next(self):
        """ Return the next event """
        self.evt = Event()
        if True:
            # if saved_line, the current line will be the previous one
            # otherwise read the next line from the trace file
            if self.saved_line:
                line = self.saved_line
                self.saved_line = None
            else:
                line = self.file_iter.next().strip("\n")

            # Process the current line
            if "TID" in line:
                self.process_event(line)
            else:
                print "[ERROR] TID not present!", line
                exit()

            while self.processed == False:
                try:
                    line = self.file_iter.next().strip("\n")
                except StopIteration:
                    self.evt.status = "NO_MORE_EVENTS"
                    # close the file handle
                    self.file_iter.close()
                    self.event_id+=1
                    return self.evt

                # TODO: Add a StopIteration exception handler
                #       for the line above
                # print "line", line
                if "TID" in line:
                    self.process_event(line)

        #except:
        if False:
            self.evt.status = "NO_MORE_EVENTS"
            return self.evt

        self.processed = False
        self.event_id+=1
        return self.evt

    def process_INS_event(self,line):
        """ Process the INStruction event """

        #print "process_INS_event", line

        # evaluate the status of the current line
        if self.evt.status == "INS" or self.evt.status == "WRITE":
            self.saved_line = line
            return self.process_Done_event(line)

        # This is an INS-truction event
        self.evt.status="INS"

        # TYPES:
        #   - BASE
        #   - X87
        #   - SSE2   <---- TODO
        #   - XSAVE  <---- TODO
        #   - SSE
        #   - SSSE3

        # get address
        address_start = line.find("0x")
        address_end   = line.find(" ",address_start)
        address       = int(line[address_start:address_end],16)
        self.evt.address=address

        # if the OS is Linux then the path is like
        # ld-linux.so.2:realloc+0x0587

        self.evt.path = line.split()[3]

        if "BASE" in line:
            after_base   = line.find("BASE")+len("BASE")
            vertical_bar = line.find("|")
            if vertical_bar == -1:
                disasm = line[after_base:].strip()
            else:
                disasm = line[after_base:vertical_bar].strip()
            self.evt.disasm=disasm
        elif "X87" in line:
            after_base   = line.find("X87")+len("X87")
            vertical_bar = line.find("|")
            if vertical_bar == -1:
                raise
            disasm = line[after_base:vertical_bar].strip()
            self.evt.disasm=disasm
        elif "SSE2" in line:
            # TODO!!!
            after_base   = line.find("SSE2")+len("SSE2")
            vertical_bar = line.find("|")
            if vertical_bar == -1:
                disasm = line[after_base:].strip()
            else:
                disasm = line[after_base:vertical_bar].strip()
            self.evt.disasm=disasm
            return
        elif "SSE" in line:
            # TODO!!!
            # NOTE: "SSE" needs to come *after* the "SSE2" case
            # order matters!
            after_base   = line.find("SSE")+len("SSE")
            vertical_bar = line.find("|")
            if vertical_bar == -1:
                disasm = line[after_base:].strip()
            else:
                disasm = line[after_base:vertical_bar].strip()
            self.evt.disasm=disasm
            return
        elif "XSAVE" in line:
            # TODO!!!
            after_base   = line.find("XSAVE")+len("XSAVE")
            vertical_bar = line.find("|")
            if vertical_bar == -1:
                raise
            disasm = line[after_base:vertical_bar].strip()
            self.evt.disasm=disasm
            return
        else:
            print '[ERROR] - UNKNOWN format!', line
            return

        if disasm == "sysenter" or disasm == "int 0x2b":
            # sysenter                             | returning 0
            # sysenter is an exception
            #print disasm
            return
        elif disasm == "int 0x80":
            self.evt.disasm = disasm
            self.evt.modified = {}
            return

        if vertical_bar != -1:
            modified_context = line[vertical_bar+1:].strip().split(",")
            self.evt.modified={}
            for modified in modified_context:
                assignment = modified.split("=")
                if len(assignment) != 2:
                    print "ERROR at processing the INS event", line
                reg   = assignment[0].strip()
                value = assignment[1].strip()
                # add reg and value to the event
                self.evt.modified[reg]=value
        return

    def process_Read_event(self,line):
        if self.evt.status == "WRITE" or self.evt.status == "INS":
            self.saved_line = line
                #print "saved"
            return self.process_Done_event(line)

        # Set event status
        self.evt.status="READ"

        #------ examples
        # Read event TID0: Read 0x7c97d600 = *(UINT32*)0007FDB8
        # Read event TID0: Read 0 = *(UINT32*)7C97D614
        # Read event TID0: Read 0xffffffff = *(UINT32*)7C97D604
        #----------

        # read value
        read_start = line.find("Read ")+len("Read ")
        read_end   = line.find(" =",read_start)
        read       = line[read_start:read_end]
        # translate from string to integer
        # if '0x' in read:
        #     read = int(read,16)
        # else:
        #     read = int(read)


        #size
        size_start = line.find("*(UINT")+len("*(UINT")
        if "UINT128" not in line:
            size_end   = line.find("*)")
        else:
            size_end   = line.find(")")
        #print line, size_start, size_end
        size       = int(line[size_start:size_end])


        # address
        address_start = size_end+len("*)")
        address    = line[address_start:]

        #print line, ">>>",address,size,read

        # save information
        self.evt.reads.append(MemoryRead(address, size, read))
        return

    def process_Write_event(self,line):
        #print "process_Write_event", self.evt.status, line
        #----- examples
        # TID0: Write *(UINT32*)0007F59C = 0x7f660
        # TID0: Write *(UINT32*)0007F570 = 0
        # TID0: Write *(UINT32*)0007F58C = 0xffffffff
        self.evt.status="WRITE"

        # size
        size_start = line.find("*(UINT")+len("*(UINT")
        size_end   = line.find("*)")
        size       = line[size_start:size_end]

        # address
        address_start = size_end+len("*)")
        address_end   = line.find(" ",address_start)
        address       = line[address_start:address_end]

        # value
        value_start = line.find("= ")+len("= ")
        value = line[value_start:]
        #print 'v:',value, line
        #print int(value,16)

        if 'UINT128' in line:
            #print line
            #print "address,size,value", address, size, value, ">>", line
            pass

        self.evt.writes.append(MemoryWrite(address,size,value))
        return

    def process_Done_event(self,line):
        # this is called when the current instruction is fully processed
        # that means status [READ]INS[WRITE]

        self.processed = True
        return self.evt

    # We assume here that every line starts with the TIDnn format
    def process_event(self,line):

        first_colon = line.find(":")
        tid = line[3:first_colon]
        self.evt.tid = int(tid,10)
        etype = line[first_colon+2:line.find(" ",first_colon+2)]

        if  etype == "INS":
            self.process_INS_event(line)
        elif etype == "Read":
            self.process_Read_event(line)
        elif etype == "Write":
            self.process_Write_event(line)
        else:
            # TODO TODO TODO!!!
            # print "[ERROR] Unhandled event", etype, line
            pass
        # event is processed
        # now proceed to analysis
        # pprinte(event)
        return self.evt


def patch_file_byte(old_file, new_file, offset):
    # open original file
    old = open(old_file,'rb')
    fbuffer = old.read()
    old.close()

    #patch
    tbr = fbuffer[offset]
    oc = chr(one_complement(ord(tbr)))

    lbuffer = list(fbuffer)
    lbuffer[offset] = oc

    nbuffer = ''.join(lbuffer)

    # write to the new file
    target = open(new_file,'w')
    target.write(nbuffer)
    target.close()

    return


class SDEInstructionInfo(object):
    def __init__(self, address, disasm, mem_read, mem_write):
        self.address = address
        self.disasm = disasm
        self.mem_read = mem_read
        self.mem_write = mem_write


def interesting_instructions_from_trace(path, executable_name, target_function):
    trace = Trace(path)

    instructions = []

    while True:
        event = trace.next()

        address = event.address
        disasm  = event.disasm

        search_string = executable_name + ":" + target_function

        if search_string in event.path:
            # I don't think x86 instructions can have more than 1 memory read in one instruction?
            assert len(event.reads) <= 1
            assert len(event.writes) <= 1

            read = event.reads[0] if len(event.reads) > 0 else None
            write = event.writes[0] if len(event.writes) > 0 else None
            
            instructions.append(SDEInstructionInfo(address, disasm,
                                                   read, write))

        if event.status == "NO_MORE_EVENTS":
            break

    return instructions
