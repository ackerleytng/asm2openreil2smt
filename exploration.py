class Conditional(object):
    """Conditional is a branch in code
    A Conditional contains the address of the conditional branch and further Conditionals, 
    which are on the taken and not taken paths of this conditional branch

    branches is a dictionary of the taken and not taken paths, keyed by booleans 
      True: the next Conditional along the trace if this conditional branch is taken
      False: the next Conditional along the trace if this conditional branch is not taken
    """

    def __init__(self):
        self.address = None
        self.taken_branch = None
        self.not_taken_branch = None
        self.count = 1

    def add_conditional(self, branch_taken):
        """Adds a conditional to this conditional, on the branch[branch_taken]
        """
        if branch_taken:
            if self.taken_branch is None:
                self.taken_branch = ret_conditional = Conditional()
            else:
                self.taken_branch.count += 1
                ret_conditional = self.taken_branch
        else:
            if self.not_taken_branch is None:
                self.not_taken_branch = ret_conditional = Conditional()
            else:
                self.not_taken_branch.count += 1
                ret_conditional = self.not_taken_branch
            
        return ret_conditional

    def __str__(self):
        address = "0x{:08x}".format(self.address) if self.address is not None else "?"
        taken = self.taken_branch if self.taken_branch is not None else "()"
        not_taken = self.not_taken_branch if self.not_taken_branch is not None else "()"

        return "({}:{} {} {})".format(address, self.count, taken, not_taken)
        
    def get_exploration_task(self):
        # The presence of a Conditional on the tree indicates that it has been visited.
        # The only Conditional that will not be explored is the last one in the section in concern
        #   which should be the only time both the taken and not_taken branches are None

        taken = self.taken_branch
        not_taken = self.not_taken_branch

        if ((taken is None and not_taken is None) or         # This Conditional is the end of the section of the program
            (taken is not None and not_taken is not None)):  # This Conditional has been fully explored
            return None
        elif taken is not None and not_taken is None:
            taken_exploration_task = taken.get_exploration_task()
            if taken_exploration_task:
                return taken_exploration_task
            else:
                # Want to explore the not taken path at this branch
                return ExplorationTask(self.address, False)
        elif taken is None and not_taken is not None:
            not_taken_exploration_task = not_taken.get_exploration_task()
            if not_taken_exploration_task:
                return not_taken_exploration_task
            else:
                # Want to explore the taken path at this branch
                return ExplorationTask(self.address, True)

        raise Exception("Shouldn't get here in code")

class ExplorationTask(object):
    def __init__(self, address, take_branch):
        self.address = address
        self.take_branch = take_branch

    def __str__(self):
        return "0x{:08x}, {}".format(self.address, self.take_branch)

        
class ExplorationRecord(object):
    def __init__(self):
        self.tree = Conditional()

    def record_conditionals(self, openreilinstructioninfo_list):
        current_conditional = self.tree
        branch_destination = None
        
        for openreilinstructioninfo in openreilinstructioninfo_list:
            # print "0x{:08x}".format(openreilinstructioninfo.address), "=>", openreilinstructioninfo.disasm
            for inst in openreilinstructioninfo.openreil_instructions:
                # print inst
                if inst.op_name() is "JCC":
                    current_conditional.address = inst.addr
                    branch_destination = inst.c.get_val()
                    break
                elif branch_destination is not None:
                    # Decide to take branch or not based on address,
                    #   and 'move' to the next path
                    current_conditional = current_conditional.add_conditional(inst.addr == branch_destination)
                    branch_destination = None
                    
    def __str__(self):
        return str(self.tree)

    def get_exploration_task(self):
        return self.tree.get_exploration_task()
