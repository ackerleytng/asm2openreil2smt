import re
from pyopenreil.REIL import *
from pyopenreil.utils import asm
from itertools import izip, islice


class OpenReilInstructionInfo(object):
    """This contains all the information related to an x86 asm instruction,
    including read memory and address"""

    def __init__(self, sdeinstructioninfo):

        self.address = address = sdeinstructioninfo.address
        self.disasm = sdeinstructioninfo.disasm
        self.mem_read = sdeinstructioninfo.mem_read
        self.mem_write = sdeinstructioninfo.mem_write
        
        # Do some cleaning up
        asm_string = self.disasm.replace("ptr", "")

        # Parse the asm
        reader = asm.Reader(ARCH_X86, (asm_string, "ret"), addr=address)
        tr = CodeStorageTranslator(reader)

        self.openreil_instructions = tr.get_insn(address)
        
        
def parse_instruction_trace(trace):
    output = []
    
    # We have to parse instructions in pairs to see if branches were taken or not
    for current_inst, next_inst in izip(trace, islice(trace, 1, None)):
        if next_inst is not None:
            parsed_next_inst = ParsedInstruction(next_inst, None)
            next_address = parsed_next_inst.address
        else:
            next_address = None
            
        output.append(ParsedInstruction(current_inst, next_address))

    return output
        
