import re
import solver
import sdeparser
from os.path import isfile as file_exists
from openreilwrapper import OpenReilInstructionInfo
from exploration import ExplorationRecord
from subprocess import check_output, CalledProcessError


PATH_TO_SDE = "../sde-external-7.15.0-2015-01-11-lin/sde"


def run_sde(path, command):
    print "[+] Running sde..."
    try:
        check_output((path + " -dt_symbols -odebugtrace tmp_sdetrace.dat -- " + command).split())
        return False
    except CalledProcessError:
        return True
        

'''
Here is the list of sde instructions:
TID0: INS 0x0804853d tmp_target:test     BASE     push ebp                                 | esp = 0xbffff3c8
TID0: INS 0x0804853e tmp_target:test+0x0001 BASE     mov ebp, esp                          | ebp = 0xbffff3c8
TID0: INS 0x08048540 tmp_target:test+0x0003 BASE     sub esp, 0x28                         | esp = 0xbffff3a0, eflags = 0x286
TID0: INS 0x08048543 tmp_target:test+0x0006 BASE     mov dword ptr [ebp-0xc], 0x0
TID0: INS 0x0804854a tmp_target:test+0x000d BASE     mov eax, dword ptr [ebp+0x8]          | eax = 0xbffff3e7
TID0: INS 0x0804854d tmp_target:test+0x0010 BASE     movzx eax, byte ptr [eax]             | eax = 0x67
TID0: INS 0x08048550 tmp_target:test+0x0013 BASE     cmp al, 0x62                          | eflags = 0x206
TID0: INS 0x08048552 tmp_target:test+0x0015 BASE     jnz 0x8048558
TID0: INS 0x08048558 tmp_target:test+0x001b BASE     mov eax, dword ptr [ebp+0x8]          | eax = 0xbffff3e7
TID0: INS 0x0804855b tmp_target:test+0x001e BASE     add eax, 0x1                          | eax = 0xbffff3e8, eflags = 0x286
TID0: INS 0x0804855e tmp_target:test+0x0021 BASE     movzx eax, byte ptr [eax]             | eax = 0x6f
TID0: INS 0x08048561 tmp_target:test+0x0024 BASE     cmp al, 0x61                          | eflags = 0x202
TID0: INS 0x08048563 tmp_target:test+0x0026 BASE     jnz 0x8048569
TID0: INS 0x08048569 tmp_target:test+0x002c BASE     mov eax, dword ptr [ebp+0x8]          | eax = 0xbffff3e7
TID0: INS 0x0804856c tmp_target:test+0x002f BASE     add eax, 0x2                          | eax = 0xbffff3e9, eflags = 0x282
TID0: INS 0x0804856f tmp_target:test+0x0032 BASE     movzx eax, byte ptr [eax]             | eax = 0x6f
TID0: INS 0x08048572 tmp_target:test+0x0035 BASE     cmp al, 0x64                          | eflags = 0x202
TID0: INS 0x08048574 tmp_target:test+0x0037 BASE     jnz 0x804857a
TID0: INS 0x0804857a tmp_target:test+0x003d BASE     mov eax, dword ptr [ebp+0x8]          | eax = 0xbffff3e7
TID0: INS 0x0804857d tmp_target:test+0x0040 BASE     add eax, 0x3                          | eax = 0xbffff3ea, eflags = 0x282
TID0: INS 0x08048580 tmp_target:test+0x0043 BASE     movzx eax, byte ptr [eax]             | eax = 0x64
TID0: INS 0x08048583 tmp_target:test+0x0046 BASE     cmp al, 0x21                          | eflags = 0x202
TID0: INS 0x08048585 tmp_target:test+0x0048 BASE     jnz 0x804858b
TID0: INS 0x0804858b tmp_target:test+0x004e BASE     cmp dword ptr [ebp-0xc], 0x4          | eflags = 0x297
TID0: INS 0x0804858f tmp_target:test+0x0052 BASE     jnz 0x804859d
TID0: INS 0x0804859d tmp_target:test+0x0060 BASE     leave                                 | ebp = 0xbffff3f8, esp = 0xbffff3cc
TID0: INS 0x0804859e tmp_target:test+0x0061 BASE     ret                                   | esp = 0xbffff3d0

The last jnz checks whether <dword ptr [ebp-0xc]> == 4.
<dword ptr [ebp-0xc]> is the accumulator, which increments if 
any of the earlier conditions comparing the memory read to the 
search pattern is met. In the trace shown above, the search pattern 
is never met, so the value <dword ptr [ebp-0xc]> is never incremented
from zero.

However, in finding the offending string, we don't need to explore 
the jnz's (0x0804858f) branches, which explains why the last three 
instructions in the list is dropped from the processing
'''

#----------------------------------------------------------------------
# Script begins here
# There is a significant amount of hard coding in this script
#   If this is to really be applied, more automation should be done
#----------------------------------------------------------------------
memory_offset_for_input = 0xbffff2e7
inputs = ["good"]
record = ExplorationRecord()

while True:
    print "[+] Trying input:", inputs[-1]
    
    # Write input to input file
    with open("tmp_input.txt", "w") as f:
        f.write(inputs[-1])
        
    # Run program and in sde to get trace
    print "[+] Tracing..."
    if run_sde(PATH_TO_SDE, "./tmp_target tmp_input.txt"):
        print "[+] Found bug: offending input:", inputs[-1]
        break
    
    # Get interesting instructions from trace
    print "[+] Parsing trace..."
    sdeinstructioninfo_list = sdeparser.interesting_instructions_from_trace("./tmp_sdetrace.dat", "tmp_target", "test")

    # Drop unnecessary instructions (see note above)
    openreilinstructioninfo_list = [OpenReilInstructionInfo(i) for i in sdeinstructioninfo_list[:-3]]
    
    # Note the execution path down in an ExplorationRecord
    record.record_conditionals(openreilinstructioninfo_list)
    print "[+] Updated tree:", record
    
    # Ask for an exploration task
    exploration_task = record.get_exploration_task()
    if exploration_task is None:
        print "[-] No bug found, tried", inputs
        break
    print "[+] Solving for:", exploration_task

    # Solve for exploration task
    s = solver.Solver(openreilinstructioninfo_list)
    filtered, all_results, stdout = s.solve(exploration_task)

    new_input = list(inputs[-1])
    if all_results:
        for k, v in all_results.iteritems():
            if k.startswith("bv_mem") and all_results[k] is not 0:
                regex_findings = re.search(r'bv_mem_(0x[0-9a-f]+)<\d+>', k)
                if regex_findings:
                    solved_address = int(regex_findings.group(1), 0)
                    new_input[solved_address - memory_offset_for_input] = chr(all_results[k])
    else:
        print "[-] SMT Error"
        break

    # Update inputs
    inputs.append(''.join(new_input))
