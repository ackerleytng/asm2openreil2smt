class Expression(object):
    def __init__(self, function, arguments):
        self.function = function

        assert type(arguments) is list

        if len(arguments) == 2:
            if arguments[0].size < arguments[1].size:
                # Destination smaller than source, so we extract values from the second expression
                arguments[1] = ExpressionExtract(arguments[1], arguments[0].size)
            elif arguments[0].size > arguments[1].size:
                # Destination bigger than source, so we zero extend (specified in the openreil github page)
                arguments[1] = ExpressionExtend(arguments[1], arguments[0].size)
                
        # Arguments should be Expressions themselves.
        self.arguments = arguments

    @property
    def num_arguments(self):
        if self.arguments is None:
            return 0
        else:
            return len(self.arguments)

    @property
    def size(self):
        """Calculates the output size of this expression"""
        sizes = sorted(set([argument.size for argument in self.arguments]))

        assert len(sizes) == 1

        return sizes[0]

    def __str__(self):
        if self.num_arguments == 0:
            return "({})".format(self.function)
        else:
            return "({} {})".format(self.function, ' '.join(map(str, self.arguments)))

class ExpressionExtend(Expression):
    def __init__(self, expression, target_size):
        assert target_size > expression.size
        self.expression = expression
        self.target_size = target_size

    @property
    def size(self):
        return self.target_size

    @property
    def extended_amount(self):
        return self.target_size - self.expression.size
        
    def __str__(self):
        return "((_ zero_extend {}) {})".format(self.extended_amount, str(self.expression))

    
class ExpressionExtract(Expression):
    def __init__(self, expression, target_size):
        assert target_size < expression.size
        self.expression = expression
        self.target_size = target_size

    @property
    def size(self):
        return self.target_size

    def __str__(self):
        return "((_ extract {} 0) {})".format(self.target_size-1, str(self.expression))


class ExpressionComparison(Expression):
    def __init__(self, comparison_type, arguments):
        self.arguments = arguments
        sizes = sorted(set([argument.size for argument in arguments]))
        assert len(sizes) == 1

        if comparison_type == "LT":
            self.function = "bvult"
        elif comparison_type == "EQ":
            self.function = "="

    @property
    def size(self):
        # By definition
        return 1

        
    def __str__(self):
        return "(ite ({} {}) #b1 #b0)".format(self.function, ' '.join(map(str, self.arguments)))
    
class Symbol(Expression):
    def __init__(self, value, size):
        self.value = value
        self.symbol_size = size

    @property
    def size(self):
        return self.symbol_size

    def __str__(self):
        return str(self.value)

class SymbolNumber(Expression):
    def __init__(self, value, size):
        self.value = value
        self.symbol_size = size

    @property
    def size(self):
        return self.symbol_size

    def __str__(self):
        return "#b" + "{:b}".format(self.value).zfill(self.size)
    

