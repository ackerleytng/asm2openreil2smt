import re
import record
from expression import Expression, ExpressionComparison, Symbol, SymbolNumber
from subprocess import Popen, PIPE, STDOUT, check_output


class Writer():
    def __init__(self):
        self.record = record.Record()
        self.lines = []

    def __str__(self):
        together = [str(self.record)] + map(str, self.lines)
        together.append("(check-sat)")
        together.append("(get-model)")
        return "\n".join(together)

    def assign(self, arguments_for_equals):
        self.lines.append(Expression("assert",[Expression("=", arguments_for_equals)]))
    
    def translate(self, openreilinstructioninfo):
        self.lines.append(";;;; " + openreilinstructioninfo.disasm)
        for instruction in openreilinstructioninfo.openreil_instructions:
            # self.lines.append("; " + str(instruction))
            self.translate_openreil_instruction(instruction,
                                                openreilinstructioninfo.mem_read,
                                                openreilinstructioninfo.mem_write)

    def translate_openreil_instruction(self, openreil_instruction, mem_read, mem_write):
        # Place asm instruction addresses into the openreil args for convenient parsing later
        openreil_instruction.a.inst_addr = openreil_instruction.addr
        openreil_instruction.c.inst_addr = openreil_instruction.addr
        if openreil_instruction.b is not None:
            openreil_instruction.b.inst_addr = openreil_instruction.addr
        
        openreil_to_smt_function = { 'ADD' : 'bvadd',
                                               'SUB' : 'bvsub',
                                               'OR' : 'bvor',
                                               'MUL' : 'bvmul',
                                               'AND' : 'bvand',
                                               'XOR' : 'bvxor',
                                               'SHL' : 'bvshl',
                                               'SHR' : 'bvlshr',
                                             }
        
        if openreil_instruction.op_name() is "STR":
            operands = [self.translate_openreil_arg(openreil_instruction.c, True),
                        self.translate_openreil_arg(openreil_instruction.a, False)]
            self.assign(operands)
        # Arithmetic 2 operand instructions
        elif openreil_instruction.op_name() in openreil_to_smt_function.keys():
            insert = [self.translate_openreil_arg(openreil_instruction.c, True),
                      Expression(openreil_to_smt_function[openreil_instruction.op_name()],
                                 [self.translate_openreil_arg(openreil_instruction.a, False),
                                 self.translate_openreil_arg(openreil_instruction.b, False)])]
            self.assign(insert)
        elif openreil_instruction.op_name() is "NOT":
            dst = self.translate_openreil_arg(openreil_instruction.c, True)
            src = self.translate_openreil_arg(openreil_instruction.a, False)
            expression = Expression("bvnot", [src])
            self.assign([dst, expression])
        elif openreil_instruction.op_name() in ["LT", "EQ"]:
            dst = self.translate_openreil_arg(openreil_instruction.c, True)
            srca = self.translate_openreil_arg(openreil_instruction.a, False)
            srcb= self.translate_openreil_arg(openreil_instruction.b, False)
            expression = ExpressionComparison(openreil_instruction.op_name(), [srca, srcb])
            self.assign([dst, expression])
        elif openreil_instruction.op_name() is "LDM":
            if not mem_read:
                self.lines.append("MEMORY INFO MISSING: " + str(openreil_instruction))
                return
                
            if not self.record.exists("mem_" + mem_read.address):
                # Feeding the data into the system
                insert = [self.translate_openreil_arg(openreil_instruction.a, True, ),
                          SymbolNumber(int(mem_read.data, 0), int(openreil_instruction.a.size_name()))]
                self.assign(insert)
                
            # Instruction
            insert = [self.translate_openreil_arg(openreil_instruction.c, True),
                      self.translate_openreil_arg(openreil_instruction.a, False, mem_read)]
            self.assign(insert)
        elif openreil_instruction.op_name() is "STM":
            if not mem_write:
                self.lines.append("MEMORY INFO MISSING: " + str(openreil_instruction))
                return            

            insert = [self.translate_openreil_arg(openreil_instruction.c, True, mem_write),
                      self.translate_openreil_arg(openreil_instruction.a, False)]
            self.assign(insert)
        elif openreil_instruction.op_name() is "JCC":
            # JCC doesn't really translate to an instruction in smt
            self.lines.append("JCC!: " + str(openreil_instruction))
        else:
            self.lines.append("UNIMPLEMENTED: " + str(openreil_instruction))

        self.record.update()

    def translate_openreil_arg(self, openreil_arg, is_dst, mem_info=None):
        """is_dst tells this translator if it should be getting a new variable from record"""

        REIL_NAMES_ARG = [ 'NONE', 'REG', 'TEMP', 'CONST' ]
        if REIL_NAMES_ARG[openreil_arg.type] == 'CONST':
            return SymbolNumber(openreil_arg.get_val(), int(openreil_arg.size_name()))
        elif REIL_NAMES_ARG[openreil_arg.type] != 'NONE':
            if mem_info:
                root = "mem_" + mem_info.address
            elif REIL_NAMES_ARG[openreil_arg.type] == 'TEMP':
                # This attribute was monkey-patched into openreil_arg
                root = openreil_arg.name + "_0x{:08x}".format(openreil_arg.inst_addr)
            else:
                # 'REG'
                root = openreil_arg.name
            
            if is_dst:
                return Symbol(self.record.get_new(root, int(openreil_arg.size_name())),
                              int(openreil_arg.size_name()))
            else:
                return Symbol(self.record.get_last_used(root, int(openreil_arg.size_name())),
                              int(openreil_arg.size_name()))
        
    def run_z3(self, path, smt):
        p = Popen([path, '-smt2', '-in'], stdout=PIPE, stdin=PIPE, stderr=PIPE)    
        stdout, stderr = p.communicate(input=str(smt))

        # Parsing output
        data = str(stdout).replace('\n','')
        data = re.sub(r' +', ' ', data)

        if re.search(r'error', data):
            return None, None, stdout
    
        results = re.findall(r'define-fun (.*?) \(\) \(.*?\) (#[xb][0-9a-f]+)', data)
        all_results = dict(results)

        final_register_values = { k:v for (k,v) in all_results.iteritems()
                                  if k.startswith("bv_R") or k.startswith("bv_mem") }

            
        return final_register_values, all_results, stdout

