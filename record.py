class VariableTracker(object):
    def __init__(self, root, size=32):
        self.root = root
        self.size = size
        self.version = 0
        self.version_next = 0

    def update(self):
        """Called after processing of one instruction is complete. This will update version to version_next if this variable was used during the processing of the current instruction"""
        self.version = self.version_next

    def get_new_name(self):
        self.version_next += 1
        return self.name(self.version_next)
        
    def get_name(self):
        return self.name(self.version)

    def name(self, i):
        return "bv_" + self.root + "<" + str(i) + ">"

    def __str__(self):
        return "\n".join(["(declare-const {} (_ BitVec {:d}))".format(self.name(i), self.size)
                          for i in range(self.version+1)])
        

class Record(object):
    def __init__(self):
        self.storage = dict()

    def update(self):
        for k, v in self.storage.iteritems():
            v.update()

    def exists(self, root):
        """If the variable has been used before, return the VariableTracker,
        If not, return None"""
        return self.storage.get(root)
    
    def get_last_used(self, root, size=32):
        if not self.exists(root):
            self.storage[root] = VariableTracker(root, size)

        return self.storage[root].get_name()
    
    def get_new(self, root, size=32):
        if not self.exists(root):
            self.storage[root] = VariableTracker(root, size)
            return self.storage[root].get_name()
        else:
            return self.storage[root].get_new_name()
        
    def __str__(self):
        return "\n".join([str(v) for k, v in self.storage.iteritems()])
